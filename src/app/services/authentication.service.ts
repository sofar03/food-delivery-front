import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Hosts } from '../utils/Hosts';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(
    private http: HttpClient) { }

  login(auth: { email: string, password: string }): Observable<void> {
    return this.http.post<void>(Hosts.localhostUrl + '/authenticate', auth, { withCredentials: true });
  }

  logout() {
    return this.http.post<void>(Hosts.localhostUrl + '/logout', {}, { withCredentials: true });
  }

}
