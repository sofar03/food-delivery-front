import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Hosts } from '../utils/Hosts';
import { Observable } from 'rxjs';
import { Product } from '../models/Product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  getGroupedProductsBySupplierId(supplierId: string): Observable<Map<String, Product>> {
    return this.http.get<Map<String, Product>>(Hosts.localhostUrl + '/api/products/' + supplierId + '/grouped');
  }

}
