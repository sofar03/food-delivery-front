import { TestBed } from '@angular/core/testing';

import { AuthenticationListenerService } from './authentication-listener.service';

describe('AuthenticationListenerService', () => {
  let service: AuthenticationListenerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthenticationListenerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
