import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationListenerService {

  private _authenticationStatusSource = new Subject<boolean>();
  status$ = this._authenticationStatusSource.asObservable();
  
  constructor() { }

  changeStatus(status: boolean) {
    this._authenticationStatusSource.next(status);
  }

}
