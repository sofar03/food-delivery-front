import { Injectable } from '@angular/core';
import { User } from '../models/User';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Hosts } from '../utils/Hosts';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private router: Router,
    private http: HttpClient) { }

  save(body: any): Observable<User> {
    return this.http.post<User>(Hosts.localhostUrl + '/api/users/new', body);
  }

  me(): Observable<User> {
    return this.http.get<User>(Hosts.localhostUrl + '/api/users/me', { withCredentials: true });
  }

  getAllUsers(): Observable<User[]> {
    return this.http.get<User[]>(Hosts.localhostUrl + '/api/users');
  }

  getUserById(userId: string): Observable<User> {
    return this.http.get<User>(Hosts.localhostUrl + '/api/users/' + userId);
  }

  update(body: any, userId: string) {
    return this.http.put<User>(Hosts.localhostUrl + '/api/users/' + userId + '/update', body);
  }

}
