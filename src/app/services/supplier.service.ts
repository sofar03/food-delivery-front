import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Order } from '../models/Order';
import { Observable } from 'rxjs';
import { Hosts } from '../utils/Hosts';
import { Supplier } from '../models/Supplier';

@Injectable({
  providedIn: 'root'
})
export class SupplierService {

  constructor(private http: HttpClient) { }

  getAllTags() {
    return this.http.get<string[]>(Hosts.localhostUrl + '/api/suppliers/allTags');
  }

  getSupplierById(id: string){
    return this.http.get<Supplier>(Hosts.localhostUrl + '/api/suppliers/byId/' + id);
  }

  getSupplierByTag(tag: string) {
    return this.http.get<Supplier[]>(Hosts.localhostUrl + '/api/suppliers/byTag/' + tag);
  }

  getAllSuppliers() {
    return this.http.get<Supplier[]>(Hosts.localhostUrl + '/api/suppliers');
  }

  getAllSuppliersCompact() {
    return this.http.get<Supplier[]>(Hosts.localhostUrl + '/api/suppliers/compact');
  }

  getTopSuppliers() {
    return this.http.get<Supplier[]>(Hosts.localhostUrl + '/api/suppliers/topSuppliers?limit=10');
  }

}
