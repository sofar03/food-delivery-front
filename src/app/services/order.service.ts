import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Order } from '../models/Order';
import { HttpClient } from '@angular/common/http';
import { Hosts } from '../utils/Hosts';
import { ProductEntry } from '../models/ProductEntry';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http: HttpClient) { }

  createOrder(userId: string) {
    return this.http.post<Order>(Hosts.localhostUrl + '/api/orders/new', {'userId': userId});
  }

  addProduct(orderId: string, productId: string) {
    return this.http.post<Order>(Hosts.localhostUrl + '/api/orders/' + orderId + '/add/product', productId);
  }

  updateStatus(orderId: string, status: string) {
    return this.http.post<Order>(Hosts.localhostUrl + '/api/orders/' + orderId + '/update/status', status);
  }

  deleteProduct(orderId: string, productId: string) {
    return this.http.delete<Order>(Hosts.localhostUrl + '/api/orders/' + orderId + '/delete/product/' + productId);
  }

  getOrdersByUserId(userId: string) {
    return this.http.get<Order[]>(Hosts.localhostUrl + '/api/orders/byUserId/' + userId);
  }

  getCurrentOrderByUserId(userId: string) {
    return this.http.get<Order>(Hosts.localhostUrl + '/api/orders/getCurrent/' + userId);
  }
  
}
