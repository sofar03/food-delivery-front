import { TestBed } from '@angular/core/testing';

import { ProductListenerService } from './product-listener.service';

describe('ProductListenerService', () => {
  let service: ProductListenerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProductListenerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
