import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Hosts } from '../utils/Hosts';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HealthCheckService {

  statusSource = new Subject<boolean>();
  status$ = this.statusSource.asObservable();

  constructor(
    private http: HttpClient
  ) { }

  checkHealth() {
    setInterval(() => {
      this.http.get<void>(Hosts.localhostUrl + '/actuator/health').subscribe(
        reps => {
          this.setStatus(true);
        },
        error => {
          this.setStatus(false);
        }
      );
    }, 20000);
  }

  setStatus(status: boolean) {
      this.statusSource.next(status);
  }

}
