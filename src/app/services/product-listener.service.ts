import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Product } from '../models/Product';

@Injectable({
  providedIn: 'root'
})
export class ProductListenerService {

  private _productAddSource = new Subject<Product>();
  product$ = this._productAddSource.asObservable();
  
  constructor() { }

  addProduct(product: Product) {
    this._productAddSource.next(product);
  }

}
