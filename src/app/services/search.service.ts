import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SearchTip } from '../models/SearchTip';
import { Hosts } from '../utils/Hosts';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(
    private http: HttpClient) { }

  getAllTips() {
    return this.http.get<SearchTip[]>(Hosts.localhostUrl + '/api/search/allTips');
  }

}
