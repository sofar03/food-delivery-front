import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from '../services/user.service';
import { User } from '../models/User';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  isAuthenticated = false;
  isRequestReceived = false;

  constructor(
    private router: Router,
    private userService: UserService) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {

    return this.isUserAuthenticated()
      .catch(error => {
        this.router.navigateByUrl('/login');
        return false;
      })
      .then(resp => {
        return true;
      });
  }

  isUserAuthenticated(): Promise<User> {
    return (async () => {
      return await this.userService.me().toPromise();
    })();
  }

}
