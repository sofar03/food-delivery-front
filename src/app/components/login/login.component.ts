import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthenticationListenerService } from 'src/app/services/authentication-listener.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  isError = false;

  constructor(
    private router: Router,
    private snackBar: MatSnackBar,
    private authenticationListenerService: AuthenticationListenerService,
    private authenticationService: AuthenticationService,
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: [
        '',
        [
          Validators.email,
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$'),
          Validators.minLength(8),
          Validators.maxLength(64),
        ]
      ],
      password: [
        '',
        [
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(64),
        ]
      ],
    });
  }

  login() {
    this.authenticationService.login({
      email: this.loginForm.value.email,
      password: this.loginForm.value.password
    })
    .subscribe(
      resp => {
        this.router.navigateByUrl('');
        this.authenticationListenerService.changeStatus(true);
      },
      error => {
        this.isError = true;
        this.authenticationListenerService.changeStatus(false);
        this.snackBar.open('Incorrect email or password', '', { duration: 1000 });
      }
    );
  }

}
