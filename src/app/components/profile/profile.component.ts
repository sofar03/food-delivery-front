import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/User';
import { UserService } from 'src/app/services/user.service';
import { OrderService } from 'src/app/services/order.service';
import { Order } from 'src/app/models/Order';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  user: User;
  orders: Order[];

  constructor(
    private userService: UserService,
    private orderService: OrderService
  ) { }

  ngOnInit(): void {
    this.userService.me().subscribe((resp) => {
      this.user = resp;

      this.orderService.getOrdersByUserId(this.user.id).subscribe((resp) => {
        this.orders = resp;
        console.log(this.orders);
      });
    });
  }

}
