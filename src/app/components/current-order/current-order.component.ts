import { Component, OnInit, EventEmitter, Output, Inject } from '@angular/core';
import { ProductListenerService } from 'src/app/services/product-listener.service';
import { Product } from 'src/app/models/Product';
import { ProductService } from 'src/app/services/product.service';
import { Order } from 'src/app/models/Order';
import { ProductEntry } from 'src/app/models/ProductEntry';
import { OrderService } from 'src/app/services/order.service';
import { catchError } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/User';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-current-order',
  templateUrl: './current-order.component.html',
  styleUrls: ['./current-order.component.css']
})
export class CurrentOrderComponent implements OnInit {

  order: Order;
  user: User;

  @Output() orderReadyEvent = new EventEmitter();

  constructor(
    private orderDialog: MatDialog,
    private userService: UserService,
    private productListenerService: ProductListenerService,
    private orderService: OrderService) { }

  ngOnInit(): void {
    this.userService.me().subscribe(resp => {
      this.user = resp;

      this.orderService.getCurrentOrderByUserId(this.user.id)
        .subscribe(
          resp => {
            this.order = resp;
            this.orderReadyEvent.emit();
          },
          err => {
            this.orderService.createOrder(this.user.id).subscribe(resp => {
              this.order = resp;
              this.orderReadyEvent.emit();
            });
          });

      this.productListenerService.product$.subscribe(product => {
        this.orderService.addProduct(this.order.id, product.id).subscribe(resp => {
          this.order = resp;
        });
      })
    });
  }

  incrementProduct(productEntry: ProductEntry) {
    this.orderService.addProduct(this.order.id, productEntry.product.id).subscribe(resp => {
      this.order = resp;
    });
  }

  decrementProduct(productEntry: ProductEntry) {
    this.orderService.deleteProduct(this.order.id, productEntry.product.id).subscribe(resp => {
      this.order = resp;
    });
  }

  doneOrder() {
    if (this.order.products.length > 0) {
      this.orderService.updateStatus(this.order.id, 'DONE').subscribe(resp => {
        this.order = null;
        this.ngOnInit();
        this.orderDialog.open(OrderDialog, {
          width: '300px',
          data: 'Thanks for your order. But currently, the courier module isn\'t ready yet, so your order status will be set to Done. Enjoy nothing...'
        })
      });
    } else {
      this.orderDialog.open(OrderDialog, {
        width: '300px',
        data: 'You must add some product to your order...'
      })
    }
  }

}

@Component({
  selector: 'order-dialog',
  templateUrl: 'order-dialog.component.html',
})
export class OrderDialog {

  constructor(
    public dialogRef: MatDialogRef<OrderDialog>,
    @Inject(MAT_DIALOG_DATA) public data: string) {}

}