import { Component, OnInit } from '@angular/core';
import { Supplier } from 'src/app/models/Supplier';
import { SupplierService } from 'src/app/services/supplier.service';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-suppliers',
  templateUrl: './suppliers.component.html',
  styleUrls: ['./suppliers.component.css']
})
export class SuppliersComponent implements OnInit {

  suppliers: Supplier[];
  tags: string[];

  colorsMap = new Map<String, String>();
  colors = [ '#f4a261', '#2a9d8f', '#e5989b', '#ffb4a2', '#02c39a', '#a8dadc', '#ff6b6b', '#e07a5f', '#a0c4ff', '#84a98c', '#b56576' ];
  currentTag = 'All';

  constructor(
    private router: Router,
    private supplierService: SupplierService) { }

  ngOnInit(): void {
    this.supplierService.getAllSuppliersCompact().subscribe(resp => {
      this.suppliers = resp;
      
      this.supplierService.getAllTags().subscribe(resp => {
        this.tags = resp;
      })
    });
  }

  goToSupplier(topSupplier: Supplier) {
    this.router.navigateByUrl('suppliers/' + topSupplier.id);
  }

  getRandomColor(tag: string) {
    if (this.colorsMap.has(tag)) {
      return this.colorsMap.get(tag);
    } else {
      let color = this.colors[Math.floor(Math.random() * ((this.colors.length - 1)))];
      this.colorsMap.set(tag, color);
      return this.colorsMap.get(tag);
    }
  }

  setCurrentTag(tag: string) {
    this.currentTag = tag;
    this.supplierService.getSupplierByTag(tag.toUpperCase().replace(' ', '_')).subscribe(resp => {
      this.suppliers = resp;
    });
  }

}
