import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/User';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';
import { AuthenticationListenerService } from 'src/app/services/authentication-listener.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  isAuthenticated = false;

  constructor(
    private router: Router,
    private authenticationListenerService: AuthenticationListenerService,
    private authenticationService: AuthenticationService,
    private userService: UserService) { }

  ngOnInit(): void {
    this.userService.me().subscribe(
      reps => {
        this.authenticationListenerService.changeStatus(true);
      },
      error => {
        this.authenticationListenerService.changeStatus(false);
      }
    );
    this.authenticationListenerService.status$.subscribe(status => {
      this.isAuthenticated = status;
    });
  }

  logout() {
    this.authenticationListenerService.changeStatus(false);
    this.authenticationService.logout().subscribe((resp) => {
      this.router.navigateByUrl('/login');
    });
  }

}
