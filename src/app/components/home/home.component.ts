import { Component, OnInit } from '@angular/core';
import { SupplierService } from 'src/app/services/supplier.service';
import { Supplier } from 'src/app/models/Supplier';
import { FormControl, FormBuilder, FormGroup } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { SearchService } from 'src/app/services/search.service';
import { SearchTip } from 'src/app/models/SearchTip';
import { HealthCheckService } from 'src/app/services/health-check.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  topSuppliers: Supplier[];

  myControl = new FormControl();
  searchForm: FormGroup;

  tips: SearchTip[];

  filteredOptions: Observable<SearchTip[]>;

  isLoaded = false;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private searchService: SearchService,
    private healthCheckService: HealthCheckService,
    private supplierService: SupplierService) { }

  ngOnInit(): void {
    this.healthCheckService.checkHealth();
    this.healthCheckService.status$.subscribe(status => {
      this.isLoaded = status;
    });

    this.searchForm = this.formBuilder.group({
      searchValue: ''
    });

    this.searchService.getAllTips().subscribe(
      resp => {
        this.tips = resp;
        this.isLoaded = true;
      }
    );

    this.supplierService.getTopSuppliers().subscribe(
      reps => {
        this.topSuppliers = reps;
        this.isLoaded = true;
      }
    );

    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => typeof value === 'string' ? value : value.tip),
        map(name => name ? this._filter(name) : this.tips.slice())
      );
  }

  displayFn(tip: SearchTip): string {
    return tip && tip.tip ? tip.tip : '';
  }

  private _filter(name: string): SearchTip[] {
    const filterValue = name.toLowerCase();

    return this.tips.filter(tip => tip.tip.toLowerCase().indexOf(filterValue) === 0);
  }

  goToSupplier(topSupplier: Supplier) {
    this.router.navigateByUrl('suppliers/' + topSupplier.id);
  }

  search() {
    console.log(this.myControl.value);

    if (typeof this.myControl.value === 'string') {
      let arr = this.tips.filter(t => t.tip === this.myControl.value);
      if (arr[0] !== undefined) {
        this.router.navigateByUrl('suppliers/' + arr[0].id);
      } else {
        this.snackBar.open('This restaurant doesn\'t exists', '', { duration: 1500 });
      }
    } else if (this.myControl.value.id !== undefined) {
      let arr = this.tips.filter(t => t.id === this.myControl.value.id);
      if (arr[0] !== undefined) {
        this.router.navigateByUrl('suppliers/' + arr[0].id);
      }
    }
  }

}
