import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Supplier } from 'src/app/models/Supplier';
import { Router, ActivatedRoute } from '@angular/router';
import { SupplierService } from 'src/app/services/supplier.service';
import { Product } from 'src/app/models/Product';
import { ProductListenerService } from 'src/app/services/product-listener.service';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-supplier-details',
  templateUrl: './supplier-details.component.html',
  styleUrls: ['./supplier-details.component.css']
})
export class SupplierDetailsComponent implements OnInit {

  supplier: Supplier;
  products: Map<String, Product>;
  supplierId: string;
  isOrderReady: boolean;

  constructor(
    private router: Router, 
    private activatedRoute: ActivatedRoute,
    private supplierService: SupplierService,
    private productService: ProductService,
    private productListenerService: ProductListenerService) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      // Get id param from url
      this.supplierId = params['id'];

      // Get supplier details
      this.supplierService.getSupplierById(this.supplierId).subscribe(resp => {
        this.supplier = resp;
      });

      this.productService.getGroupedProductsBySupplierId(this.supplierId).subscribe(resp => {
        this.products = resp;
      });
    });
  }

  addProductToOrder(product: Product) {
    this.productListenerService.addProduct(product);
  }

  setOrderReady() {
    this.isOrderReady = true;
  }

}
