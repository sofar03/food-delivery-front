import { Product } from './Product';

export class Supplier {

  id: string;
  name: string;
  address: string;
  imageUrl: string;
  products: Product[];

}