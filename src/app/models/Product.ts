import { Dimensions } from './Dimensions';

export class Product {

    id: string;
    name: string;
    productType: string;
    price: number;
    dimensions: Dimensions;
    imageUrl: string;

}