import { Order } from './Order';

export class User {

    id: string;
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    city: string;
    address: string;
    orders: Order;

}