import { ProductEntry } from './ProductEntry';

export class Order {

    id: string;
    total: number;
    status: string;
    creationDate: Date;
    products: ProductEntry[];

}