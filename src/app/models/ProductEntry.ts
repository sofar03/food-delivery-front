import { Product } from './Product';

export class ProductEntry {

  product: Product;
  amount: number;

}