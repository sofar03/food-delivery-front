export class Dimensions {

  // In centimeters
  width: number;

  // In centimeters
  height: number;

  // In centimeters
  length: number;

  // In grams
  weight: number;

}